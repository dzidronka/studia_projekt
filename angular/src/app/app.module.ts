import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {PolishDatePipe} from './pipes/polish-date.pipe';
import {FormsModule} from '@angular/forms';
import {SearchGalleriesPipe} from './pipes/search-galleries.pipe';
import {NavComponent} from './components/nav/nav.component';
import {GalleryItemComponent} from './components/galleries/gallery-item/gallery-item.component';
import {GalleriesComponent} from './components/galleries/galleries/galleries.component';
import {GallerySearchComponent} from './components/galleries/gallery-search/gallery-search.component';
import {AppRoutingModule} from './app-routing.module';
import {DashboardComponent} from './components/dashboard/dashboard/dashboard.component';
import {GalleryComponent} from './components/galleries/gallery/gallery.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {CommentFormComponent} from './components/comments/comment-form/comment-form.component';
import {GalleryTagFormComponent} from './components/galleries/gallery-tag-form/gallery-tag-form.component';
import {GalleryPhotoFormComponent} from './components/galleries/gallery-photo-form/gallery-photo-form.component';
import {GalleryFormComponent} from './components/galleries/gallery-form/gallery-form.component';
import {CommentItemComponent} from './components/comments/comment-item/comment-item.component';
import {NgxPaginationModule} from "ngx-pagination";
@NgModule({
  declarations: [
    AppComponent,
    PolishDatePipe,
    SearchGalleriesPipe,
    NavComponent,
    GalleryItemComponent,
    GalleriesComponent,
    GallerySearchComponent,
    DashboardComponent,
    GalleryComponent,
    CommentFormComponent,
    GalleryTagFormComponent,
    GalleryPhotoFormComponent,
    GalleryFormComponent,
    CommentItemComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    NgxPaginationModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
