export const Galleries = [{
  'galleryId': '1',
  'title': 'Comic Cosplays',
  'dateCreated': '2015-12-15T00:00:00+00:00',
  'thumbUrl': '../assets/image/gallery/back/comic.jpg',
  'description': 'Best comic cosplays and comic movie adaptations.',
  'tags': [{
    'tag': 'comic',
  }, {
    'tag': 'marvel',
  }, {
    'tag': 'dc',
  }],
  'photos': [{
    'photoId': '1',
    'thumbUrl': './assets/image/gallery/Comic/america.jpg',
    'imgUrl': './assets/image/gallery/Comic/america.jpg',
  }, {
    'photoId': '2',
    'thumbUrl': './assets/image/gallery/Comic/deadpool.jpg',
    'imgUrl': './assets/image/gallery/Comic/deadpool.jpg',
  }, {
    'photoId': '3',
    'thumbUrl': './assets/image/gallery/Comic/penguin.jpg',
    'imgUrl': './assets/image/gallery/Comic/penguin.jpg',
  }, {
    'photoId': '4',
    'thumbUrl': './assets/image/gallery/Comic/poison-ivy.jpg',
    'imgUrl': './assets/image/gallery/Comic/poison-ivy.jpg',
  }, {
    'photoId': '5',
    'thumbUrl': './assets/image/gallery/Comic/ratchett.jpg',
    'imgUrl': './assets/image/gallery/Comic/ratchett.jpg',
  }, {
    'photoId': '6',
    'thumbUrl': './assets/image/gallery/Comic/thor.jpg',
    'imgUrl': './assets/image/gallery/Comic/thor.jpg',
  }]
}, {
  'galleryId': '2',
  'title': 'Anime Cosplays',
  'dateCreated': '2019-12-15T00:00:00+00:00',
  'thumbUrl': '../assets/image/gallery/back/anime.jpg',
  'description': 'Gallery for manga and anime fans.',
  'tags': [{
    'tag': 'anime'
  }, {
    'tag': 'Hellsing',
  }, {
    'tag': 'Death Note'
  }],
  'photos': [
    {
      'photoId': '1',
      'thumbUrl': './assets/image/gallery/Manga/alucard.jpg',
      'imgUrl': './assets/image/gallery/Manga/alucard.jpg',
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/image/gallery/Manga/death-note.jpg',
      'imgUrl': './assets/image/gallery/Manga/death-note.jpg',
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/image/gallery/Manga/Mikasa.jpg',
      'imgUrl': './assets/image/gallery/Manga/Mikasa.jpg',
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/image/gallery/Manga/mononoke.jpg',
      'imgUrl': './assets/image/gallery/Manga/mononoke.jpg',
    },
    {
      'photoId': '5',
      'thumbUrl': './assets/image/gallery/Manga/Pokemon.jpg',
      'imgUrl': './assets/image/gallery/Manga/Pokemon.jpg',
    },
    {
      'photoId': '6',
      'thumbUrl': './assets/image/gallery/Manga/ryuk.jpg',
      'imgUrl': './assets/image/gallery/Manga/ryuk.jpg',
    }]
}, {
  'galleryId': '3',
  'title': 'Disney Cosplays',
  'dateCreated': '2005-12-15T00:00:00+00:00',
  'thumbUrl': '../assets/image/gallery/back/disney.jpg',
  'description': 'The most beautiful princessess and the most handsome princes.',
  'tags': [{
    'tag': 'disney'
  }, {
    'tag': 'fairytale',
  }, {
    'tag': 'princess'
  }],
  'photos': [
    {
      'photoId': '1',
      'thumbUrl': './assets/image/gallery/Disney/ariel.jpg',
      'imgUrl': './assets/image/gallery/Disney/ariel.jpg',
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/image/gallery/Disney/Belle.jpg',
      'imgUrl': './assets/image/gallery/Disney/Belle.jpg',
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/image/gallery/Disney/gaston.jpg',
      'imgUrl': './assets/image/gallery/Disney/gaston.jpg',
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/image/gallery/Disney/megara.jpg',
      'imgUrl': './assets/image/gallery/Disney/megara.jpg',
    },
    {
      'photoId': '5',
      'thumbUrl': './assets/image/gallery/Disney/snow-white.jpg',
      'imgUrl': './assets/image/gallery/Disney/snow-white.jpg',
    },
    {
      'photoId': '6',
      'thumbUrl': './assets/image/gallery/Disney/tinkerbell.jpg',
      'imgUrl': './assets/image/gallery/Disney/tinkerbell.jpg',
    },]
}, {
  'galleryId': '4',
  'title': 'Games Cosplays',
  'dateCreated': '2015-12-15T00:00:00+00:00',
  'thumbUrl': '../assets/image/gallery/back/games2.jpg',
  'description': 'Cosplays from PC and console games.',
  'tags': [{
    'tag': 'games'
  }, {
    'tag': 'Witcher',
  }, {
    'tag': 'Blizzard'
  }],
  'photos': [
    {
      'photoId': '1',
      'thumbUrl': './assets/image/gallery/Games/geralt.jpg',
      'imgUrl': './assets/image/gallery/Games/geralt.jpg',
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/image/gallery/Games/hellblade.jpg',
      'imgUrl': './assets/image/gallery/Games/hellblade.jpg',
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/image/gallery/Games/horizon.jpg',
      'imgUrl': './assets/image/gallery/Games/horizon.jpg',
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/image/gallery/Games/LoL.jpg',
      'imgUrl': './assets/image/gallery/Games/LoL.jpg',
    },
    {
      'photoId': '5',
      'thumbUrl': './assets/image/gallery/Games/Nier.jpg',
      'imgUrl': './assets/image/gallery/Games/Nier.jpg',
    },
    {
      'photoId': '6',
      'thumbUrl': './assets/image/gallery/Games/wow.jpg',
      'imgUrl': './assets/image/gallery/Games/wow.jpg',
    }]
}, {
  'galleryId': '5',
  'title': 'Movies Cosplays',
  'dateCreated': '2015-12-15T00:00:00+00:00',
  'thumbUrl': '../assets/image/gallery/back/movies2.jpg',
  'description': 'Cosplays from classic movies and new blockbusters.',
  'tags': [{
    'tag': 'Movies',
  }],
  'photos': [
    {
      'photoId': '1',
      'thumbUrl': './assets/image/gallery/Movies/bellatrix.jpg',
      'imgUrl': './assets/image/gallery/Movies/bellatrix.jpg',
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/image/gallery/Movies/edward.jpg',
      'imgUrl': './assets/image/gallery/Movies/edward.jpg',
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/image/gallery/Movies/maleficient.jpg',
      'imgUrl': './assets/image/gallery/Movies/maleficient.jpg',
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/image/gallery/Movies/mandalorian.jpg',
      'imgUrl': './assets/image/gallery/Movies/mandalorian.jpg',
    },
    {
      'photoId': '5',
      'thumbUrl': './assets/image/gallery/Movies/nazgul.jpg',
      'imgUrl': './assets/image/gallery/Movies/nazgul.jpg',
    },
    {
      'photoId': '6',
      'thumbUrl': './assets/image/gallery/Movies/star-trek.jpg',
      'imgUrl': './assets/image/gallery/Movies/star-trek.jpg',
    },
  ]
}, {
  'galleryId': '6',
  'title': 'Series Cosplays',
  'dateCreated': '2015-12-15T00:00:00+00:00',
  'thumbUrl': '../assets/image/gallery/back/series.jpg',
  'description': 'The best cosplays of small screen.',
  'tags': [{
    'tag': 'Netflix',
  }, {
    'tag': 'Series'
  }],
  'photos': [
    {
      'photoId': '6',
      'thumbUrl': './assets/image/gallery/Series/breakingbad.jpg',
      'imgUrl': './assets/image/gallery/Series/breakingbad.jpg',
    },
    {
      'photoId': '2',
      'thumbUrl': './assets/image/gallery/Series/drogo.jpg',
      'imgUrl': './assets/image/gallery/Series/drogo.jpg',
    },
    {
      'photoId': '3',
      'thumbUrl': './assets/image/gallery/Series/eleven.jpg',
      'imgUrl': './assets/image/gallery/Series/eleven.jpg',
    },
    {
      'photoId': '4',
      'thumbUrl': './assets/image/gallery/Series/got.jpg',
      'imgUrl': './assets/image/gallery/Series/got.jpg',
    },
    {
      'photoId': '5',
      'thumbUrl': './assets/image/gallery/Series/sherlock.jpg',
      'imgUrl': './assets/image/gallery/Series/sherlock.jpg',
    },
    {
      'photoId': '6',
      'thumbUrl': './assets/image/gallery/Series/twd.jpg',
      'imgUrl': './assets/image/gallery/Series/twd.jpg',
    },
  ]
}];
