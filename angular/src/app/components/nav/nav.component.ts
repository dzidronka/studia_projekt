import {Component, OnInit} from '@angular/core';
import {IGallery} from '../../interfaces/IGallery';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.src/assets/styles/main.scss', './nav.component.scss']
})
export class NavComponent implements OnInit {
  galleries: IGallery[];
  title: string;
  description: string;

  constructor() {
    this.title = 'COSPLAY GALLERIES';
    this.description = '...when fiction becomes reality.';
  }

  ngOnInit() {}
}
