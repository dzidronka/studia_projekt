import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {IPhoto} from '../../../interfaces/iphoto';
import * as uuid from 'uuid/v4';

@Component({
  selector: 'app-gallery-photo-form',
  templateUrl: './gallery-photo-form.component.html',
  styleUrls: ['./gallery-photo-form.component.src/assets/styles/main.scss']
})
export class GalleryPhotoFormComponent implements OnInit {
  @ViewChild('photoForm', {static: false})
  photoForm: NgForm;

  @Output() addPhoto = new EventEmitter();
  @Output() closeForm = new EventEmitter();

  photo: IPhoto;

  constructor() {
  }

  ngOnInit(): void {
    this.photo = this.generateEmptyPhoto();
  }

  onAddPhoto() {
    if (this.photoForm.valid) {
      this.addPhoto.emit(this.photo);
      this.resetForm();
    }
  }

  onCancel() {
    this.resetForm();
    this.closeForm.emit();
  }

  private generateEmptyPhoto() {
    return {
      imgUrl: '',
      thumbUrl: '',
      photoId: uuid(),
    };
  }

  private resetForm() {
    this.photo = this.generateEmptyPhoto();
    this.photoForm.resetForm();
    this.photoForm.controls['imgUrl'].setErrors(null);
    this.photoForm.controls['thumbUrl'].setErrors(null);
  }
}
