import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryPhotoFormComponent } from './gallery-photo-form.component';

describe('GalleryPhotoFormComponent', () => {
  let component: GalleryPhotoFormComponent;
  let fixture: ComponentFixture<GalleryPhotoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryPhotoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryPhotoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
