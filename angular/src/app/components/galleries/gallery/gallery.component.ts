import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IGallery} from '../../../interfaces/IGallery';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {IPhoto} from '../../../interfaces/iphoto';
import {IComment} from '../../../interfaces/IComment';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.src/assets/styles/main.scss', './gallery.scss']
})

export class GalleryComponent implements OnInit {

  galleryId: string;
  gallery: IGallery;
  comments: IComment[];

  showTagForm: boolean;
  showTagsToRemove: boolean;
  showPhotoForm: boolean;
  showPhotosToRemove: boolean;
  showCommentsForm: boolean;
  showGalleryForm: boolean;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: '88'
    })
  };

  constructor(private route: ActivatedRoute,
              private http: HttpClient) {
  }

  ngOnInit() {
    this.galleryId = this.route.snapshot.paramMap.get('galleryId');
    this.http.get('http://project.usagi.pl/gallery/' + this.galleryId, this.httpOptions).toPromise().then((response: IGallery) => {
      console.log(response);
      this.gallery = response;
      console.log(this.gallery);
    });
    this.http.get('http://project.usagi.pl/comment/byGallery/' + this.galleryId, this.httpOptions).toPromise().then(
      (response: IComment[]) => {
        this.comments = response;
      }
    );
  }

  addTag(tag: string) {
    const index = this.gallery.tags.findIndex(item => item.tag === tag);
    if (index === -1) {
      this.gallery.tags.push({tag});
      this.updateGallery();
    }
  }

  addComment($event) {
    this.comments.push($event);
    console.log(this.comments);
  }

  removeTag(tag: string) {
    const index = this.gallery.tags.findIndex(item => item.tag === tag);
    this.gallery.tags.splice(index, 1);
    this.updateGallery();
  }

  addNewPhoto(photo) {
    const index = this.gallery.photos.findIndex((item: IPhoto) => item.photoId === photo.photoId);
    if (index === -1) {
      this.gallery.photos.push(photo);
      this.updateGallery();
    }
  }

  removePhoto(photoId: any) {
    const index = this.gallery.photos.findIndex((item: IPhoto) => item.photoId === photoId);
    this.gallery.photos.splice(index, 1);
    this.updateGallery();
  }

  saveGallery(gallery) {
    this.gallery = gallery;
    this.updateGallery();
  }

  openPhotoForm() {
    this.showPhotoForm = true;
    this.showCommentsForm = false;
    this.showTagsToRemove = false;
    this.showPhotosToRemove = false;
    this.showTagForm = false;
    this.showGalleryForm = false;
  }

  closePhotoForm() {
    this.showPhotoForm = false;
  }

  openTagForm() {
    this.showTagForm = true;
    this.showCommentsForm = false;
    this.showTagsToRemove = false;
    this.showPhotosToRemove = false;
    this.showPhotoForm = false;
    this.showGalleryForm = false;
  }

  closeTagForm() {
    this.showTagForm = false;
  }

  showTags() {
    this.showTagsToRemove = true;
    this.showPhotosToRemove = false;
    this.showTagForm = false;
    this.showPhotoForm = false;
    this.showCommentsForm = false;
    this.showGalleryForm = false;
  }

  showIdOfPhotos() {
    this.showPhotosToRemove = true;
    this.showTagsToRemove = false;
    this.showTagForm = false;
    this.showCommentsForm = false;
    this.showPhotoForm = false;
    this.showGalleryForm = false;
  }

  openCommentsForm() {
    this.showCommentsForm = true;
    this.showTagForm = false;
    this.showTagsToRemove = false;
    this.showPhotosToRemove = false;
    this.showPhotoForm = false;
    this.showGalleryForm = false;
  }

  closeCommentForm() {
    this.showCommentsForm = false;
  }

  openGalleryForm() {
    this.showGalleryForm = true;
    this.showCommentsForm = false;
    this.showTagForm = false;
    this.showTagsToRemove = false;
    this.showPhotosToRemove = false;
    this.showPhotoForm = false;
  }

  closeGalleryForm() {
    this.showGalleryForm = false;
  }

  changePhoto(event) {
    document.getElementById('mainPhoto').setAttribute('src', event.target.src);
  }

  private updateGallery() {
    this.http.post('http://projekt.usagi.pl/gallery/${this.gallery.gallery.Id)',
      this.gallery, this.httpOptions).toPromise().then((response: IGallery) => {
      this.gallery = response;
    });
  }
}
