import {Component, OnInit, ViewChild} from '@angular/core';
import {IGallery} from '../../../interfaces/IGallery';
import {Galleries} from '../../../constants/galleries.constant';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NgForm} from "@angular/forms";


@Component({
  selector: 'app-galleries',
  templateUrl: './galleries.component.html',
  styleUrls: ['./galleries.component.src/assets/styles/main.scss', './galleries.component.scss']
})
export class GalleriesComponent implements OnInit {

  @ViewChild('galleryForm', { static: false })
  galleryForm: NgForm;
  galleries: IGallery[];
  searchValue: string;
  limit: number;
  currentPage: number;
  start: number;
  end: number;
  numberOfPages: any;
  collection: any;
  nextPage: number;
  previousPage: number;
  showGalleryForm: boolean;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: '88'
    })
  };

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.setCurrentPage();
    this.galleries = [];
    this.currentPage = parseInt(localStorage.getItem('galleryPage')) || 0;
    this.setCurrentPage(this.currentPage);
    this.http.get('http://project.usagi.pl/gallery', this.httpOptions).toPromise().then((response: IGallery[]) => {
      console.log(response);
      this.galleries = response;
      this.numberOfPages = Array(Math.ceil(this.galleries.length / this.limit)).fill(1);
    });
    this.searchValue = '';
  }

  setSearchValue($event) {
    this.searchValue = $event;
  }

  filterByYear(value) {
    this.searchValue = value;
  }

  exportGalleries() {
    Galleries.forEach((gallery: IGallery) => {
      delete (gallery.galleryId);

      this.http.post('http://project.usagi.pl/gallery',
        gallery, this.httpOptions).toPromise().then((response: IGallery) => {
        console.log('success', response);
        this.galleries.push(response);
        this.numberOfPages = Array(Math.ceil(this.galleries.length / this.limit)).fill(1);
      }, (errResponse) => {
        console.log('error', errResponse);
      });
    });
  }

  removeGalleries() {
    this.galleries.forEach((gallery: IGallery) => {
      this.http.post('http://project.usagi.pl/gallery/delete/' + gallery.galleryId, {},
        this.httpOptions).toPromise().then((response) => {
        this.galleries.splice(0, 1);
        this.numberOfPages = Array(Math.ceil(this.galleries.length / this.limit)).fill(1);
        console.log('success', response);
      }, (errResponse) => {
        console.log('error', errResponse);
      });
    });
  }

  removeGallery(galleryId) {
    const index = this.galleries.findIndex((gallery: IGallery) => gallery.galleryId === galleryId);
    this.http.post('http://project.usagi.pl/gallery/delete/' + galleryId, {},
      this.httpOptions).toPromise().then((response) => {
        this.galleries.splice(index, 1);
        this.numberOfPages = Array(Math.ceil(this.galleries.length / this.limit)).fill(1);
        console.log('success', response);
      },
      (errResponse) => {
        console.log('error', errResponse);
      });
  }

  saveGallery(gallery) {
    this.http.post('http://project.usagi.pl/gallery', gallery, this.httpOptions).toPromise().then((response: IGallery) => {
      console.log('success', response);
      this.galleries.push(response);
      this.numberOfPages = Array(Math.ceil(this.galleries.length / this.limit)).fill(1);
    }, (errResponse) => {
      console.log('error', errResponse);
    });
  }

  setCurrentPage(page = 0) {
    this.limit = 3;
    this.currentPage = page;
    this.start = this.currentPage * this.limit;
    this.end = this.start + 3;
    localStorage.setItem('galleryPage', this.currentPage.toString());
  }

  getPreviousPage() {
    if (this.currentPage > 0) {
      this.previousPage = this.currentPage - 1;
      this.setCurrentPage(this.previousPage);
      return this.previousPage;
    } else {
      return this.currentPage;
    }
  }

  getNextPage() {
    if ((this.currentPage + 1) < this.numberOfPages.length) {
      this.nextPage = this.currentPage + 1;
      this.setCurrentPage(this.nextPage);
      return this.nextPage;
    } else {
      return this.currentPage;
    }
  }
}
