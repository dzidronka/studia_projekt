import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {IGallery} from '../../../interfaces/IGallery';

@Component({
  selector: 'app-gallery-search',
  templateUrl: './gallery-search.component.html',
  styleUrls: ['./gallery-search.component.src/assets/styles/main.scss']
})
export class GallerySearchComponent implements OnInit {

  @Output()
  searchValue: EventEmitter < String > = new EventEmitter < String > ();
  value: string;
  galleries: IGallery[];

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: '88'
    })
  };

  constructor(private http: HttpClient) {}


  ngOnInit() {
    this.value = '';
    this.http.get('http://project.usagi.pl/gallery', this.httpOptions).toPromise().then((response: IGallery[]) => {
      console.log(response);
      this.galleries = response;
    });
  }

  onChange() {
    this.searchValue.emit(this.value);
  }
}
