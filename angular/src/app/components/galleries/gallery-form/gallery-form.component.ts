import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {IGallery} from '../../../interfaces/IGallery';

@Component({
  selector: 'app-gallery-form',
  templateUrl: './gallery-form.component.html',
  styleUrls: ['./gallery-form.component.src/assets/styles/main.scss']
})
export class GalleryFormComponent implements OnInit {

  @ViewChild('galleryForm', { static: false })
  galleryForm: NgForm;
  @Input() gallery: IGallery;
  @Output() savedGallery = new EventEmitter();
  @Output() closeForm = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.gallery = (!!this.gallery) ? JSON.parse(JSON.stringify(this.gallery)) : this.defaultGallery();
  }

  private generateEmptyGallery() {
    return {
      galleryId: this.gallery.galleryId,
      dateCreated: this.gallery.dateCreated,
      title: '',
      thumbUrl: '',
      description: '',
      tags: this.gallery.tags,
      photos: this.gallery.photos,
    };
  }

  defaultGallery() {

    const gallery: IGallery = {
      title: '',
      dateCreated: new Date(),
      thumbUrl: '',
      description: '',
      tags: [],
      photos: []
    };
    return gallery;
  }

  addGallery() {
    if (this.galleryForm.valid) {
      this.savedGallery.emit(this.gallery);
      this.resetForm();
    }
  }

  onCancel() {
    this.resetForm();
    this.closeForm.emit();
  }


  private resetForm() {
    this.gallery = this.generateEmptyGallery();
    this.galleryForm.resetForm();
    this.galleryForm.controls['title'].setErrors(null);
    this.galleryForm.controls['thumbUrl'].setErrors(null);
    this.galleryForm.controls['description'].setErrors(null);
  }

}
