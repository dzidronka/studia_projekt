import {Component, Input, OnInit, Output, Inject} from '@angular/core';
import {IGallery} from '../../../interfaces/IGallery';
import {EventEmitter} from '@angular/core';


@Component({
  selector: 'app-gallery-item',
  templateUrl: './gallery-item.component.html',
  styleUrls: ['./gallery-item.component.src/assets/styles/main.scss', './gallery-item.component.scss']
})
export class GalleryItemComponent implements OnInit {

  @Input() gallery: IGallery;
  @Output() deleteGallery: EventEmitter<String> = new EventEmitter<String>();

  constructor() {
  }

  ngOnInit() {
  }

  onDelete(galleryId: string) {
    this.deleteGallery.emit(galleryId);
  }
}
