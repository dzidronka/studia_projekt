import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryTagFormComponent } from './gallery-tag-form.component';

describe('GalleryTagFormComponent', () => {
  let component: GalleryTagFormComponent;
  let fixture: ComponentFixture<GalleryTagFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryTagFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryTagFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
