import {Component, Input, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {IComment} from '../../../interfaces/IComment';

@Component({
  selector: 'app-comment-item',
  templateUrl: './comment-item.component.html',
  styleUrls: ['./comment-item.component.src/assets/styles/main.scss']
})
export class CommentItemComponent implements OnInit {

  @Input() galleryId: string;
  @Input() comments: IComment[];


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: '88'
    })
  };

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  removeComment(commentId) {
    const index = this.comments.findIndex((comment: IComment) => comment.commentId === commentId);
    this.http.post('http://project.usagi.pl/comment/delete/' + commentId, {}, this.httpOptions).toPromise().then(
      (response) => {
        console.log('success', response);
        this.comments.splice(index, 1);
      }, (errResponse) => {
        console.log('error', errResponse);
      });
  }
}
