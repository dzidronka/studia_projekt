import {Component, OnInit, Input, ViewChild, EventEmitter, Output} from '@angular/core';
import {IComment} from 'src/app/interfaces/IComment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.src/assets/styles/main.scss']
})
export class CommentFormComponent implements OnInit {
  @ViewChild('commentForm', {static: false})
  commentForm: NgForm;
  comment: IComment;

  @Input() galleryId: string;

  @Output()
  closeForm = new EventEmitter();

  @Output() addComment = new EventEmitter();
  emailPattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z09])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: '88'
    })
  };

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.comment = this.setEmptyComment();
  }

  submitComment() {
    this.http.post('http://project.usagi.pl/comment', this.comment, this.httpOptions).toPromise().then
    ((response: IComment) => {
      console.log(response);
    });
    console.log('comment', this.comment);
     this.commentForm.resetForm();
    this.comment = this.setEmptyComment();
  }

  cancelComment() {
    // this.commentForm.resetForm();
    this.closeForm.emit();
  }

  private setEmptyComment() {
    const newDate = new Date();
    return {
      galleryId: this.galleryId,
      firstName: '',
      lastName: '',
      email: '',
      message: '',
      dateCreated: newDate
    };
  }
}
