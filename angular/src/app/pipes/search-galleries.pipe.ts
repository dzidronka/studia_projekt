import {Pipe, PipeTransform} from '@angular/core';


@Pipe({
  name: 'searchGalleries'
})

export class SearchGalleriesPipe implements PipeTransform {

  galleries: any;

  transform(value: any, args ? : any): any {
    this.galleries = value;
    args = args.toString().toLowerCase();
    if (args) {
      this.galleries = this.galleries.filter(item => (item.title.toLowerCase().indexOf(args) !== -1 ||
          item.description.toLowerCase().indexOf(args) !== -1) ||
        (item.dateCreated.indexOf(args) !== -1)
      );
    }
    return this.galleries;
  }
}
